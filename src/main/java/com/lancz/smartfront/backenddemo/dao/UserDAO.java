package com.lancz.smartfront.backenddemo.dao;

import com.lancz.smartfront.backenddemo.event.DeleteAllUserEvent;
import com.lancz.smartfront.backenddemo.mapper.UserMapper;
import com.lancz.smartfront.backenddemo.model.Car;
import com.lancz.smartfront.backenddemo.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Random;

@Component
public class UserDAO {
    Logger logger = LoggerFactory.getLogger(UserDAO.class);

    @Autowired
    UserMapper userMapper;

    private Random random = new Random();

    public List<User> getAll() {
        return userMapper.getAll();
    }

    public void deleteAll() {
        userMapper.deleteAll();
    }

    public void deleteUserById(Long id) {
        logger.info("Delete user by id: " + id);
        userMapper.deleteById(id);
    }

    public User findOneById(Long id) {
        return userMapper.findOneById(id);
    }

    public Long save(User user) {
       return userMapper.insert(user);

    }

    public User createNewUser() {

        long newId = random.nextLong();

        int newAge = random.nextInt(99);
        User user = new User(newId, "Test" + newId, newAge);

        try {
            logger.info("Create new user by id: " + newId);
            userMapper.insert(user);
        } catch (DuplicateKeyException e) {
            logger.warn("Duplicate key error, id: " + newId);
            user.setId(user.getId() + 1);
            userMapper.insert(user);
        }

        return user;
    }

    @EventListener(DeleteAllUserEvent.class)
    public User onDeleteAllUserEvent(DeleteAllUserEvent event) {
        logger.info("After deleted all user create a new one.");
        User newUser = createNewUser();

        logger.info("New user created" + newUser.toString());
        return newUser;
    }
}
