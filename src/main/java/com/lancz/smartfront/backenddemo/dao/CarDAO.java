package com.lancz.smartfront.backenddemo.dao;

import com.lancz.smartfront.backenddemo.mapper.CarMapper;
import com.lancz.smartfront.backenddemo.model.Car;
import com.lancz.smartfront.backenddemo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Random;

@Component
public class CarDAO {

    @Autowired
    private CarMapper carMapper;

    public List<Car> save(Car car) {
        carMapper.insertCar(car);

        return getAll();
    }

    public List<Car> saveUserCars(User user) {
        if (!CollectionUtils.isEmpty(user.getCars())) {
            // set cars owner
            user.getCars().forEach(car -> car.setOwner(user));

            return saveCars(user.getCars());
        }

        return getAll();
    }

    public List<Car> saveCars(List<Car> cars) {
        if (!CollectionUtils.isEmpty(cars)) {
            cars.forEach(car -> carMapper.insertCar(car));
        }

        return getAll();
    }

    public List<Car> getAll() {
        return carMapper.getAll();
    }

    public List<Car> getCarsByOwnerId(Long ownerId) {
        return carMapper.getCarsByOwnerId(ownerId);
    }

    public Car createNewRandomCar(User user) {
        Random random = new Random();

        long carId = random.nextLong();

        byte[] array = new byte[3]; // length is bounded by 7
        new Random().nextBytes(array);
        String randomLicenseChars = new String(array, Charset.forName("UTF-8"));
        Random licNumRandom = new Random(100);
        int randomLicenseNums = licNumRandom.nextInt(999);

        int carAge = random.nextInt(30);

        Car newCar = new Car(carId, randomLicenseChars + "-" + randomLicenseNums, "yellow", carAge);
        newCar.setOwner(user);

        carMapper.insertCar(newCar);

        return carMapper.getOneById(carId);
    }
}
