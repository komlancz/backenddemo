package com.lancz.smartfront.backenddemo.service.report;

import com.lancz.smartfront.backenddemo.UserNotFoundException;
import com.lancz.smartfront.backenddemo.mapper.CarMapper;
import com.lancz.smartfront.backenddemo.mapper.UserMapper;
import com.lancz.smartfront.backenddemo.model.Car;
import com.lancz.smartfront.backenddemo.model.User;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.HttpClientErrorException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

@Service
public class UserCarsReportService {
    private static final Logger logger = LoggerFactory.getLogger(UserCarsReportService.class);

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private CarMapper carMapper;

    @Value("${generated.reports.path}")
    private String generatedReportsPath;

    public byte[] exportReport(Long userId, String reportFormat) throws FileNotFoundException, JRException, UserNotFoundException {
        List<Car> cars;

        if (userId == null){
            cars = carMapper.getAllCarAndUser();
        }
        else {
            cars = carMapper.getCarsByOwnerId(userId);
        }

        if (CollectionUtils.isEmpty(cars)) {
            throw new UserNotFoundException(String.format("User not found by id %s", userId));
        }

        byte[] fileInByte = new byte[0];

        // Load file and complie it
        try {
            File file = ResourceUtils.getFile("classpath:user_and_cars.jrxml");

            JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());

            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(cars);

            Map<String, Object> parameters = new HashMap<>();

            parameters.put("createdBy", "ComLancz");

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
            switch (reportFormat.toLowerCase()) {
                case "html":
                    JasperExportManager.exportReportToHtmlFile(jasperPrint, generatedReportsPath + (userId != null? userId + "_" : "") + "user_cars.html");
                    break;
                case "pdf":
                    JasperExportManager.exportReportToPdfFile(jasperPrint, generatedReportsPath + (userId != null? userId + "_" : "") +  "user_cars.pdf");
                    break;
                case "pdf_byte":
                    fileInByte = JasperExportManager.exportReportToPdf(jasperPrint);
                    break;
            }
        } catch (FileNotFoundException e) {
            logger.error("An error occurred while read JRXML file: " + e.getMessage());

            throw e;
        } catch (JRException e) {
            logger.error("An error occurred while compile JasperReport: " + e);

            throw e;
        }

        return fileInByte;
    }
}
