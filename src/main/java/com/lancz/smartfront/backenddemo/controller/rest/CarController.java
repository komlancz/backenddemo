package com.lancz.smartfront.backenddemo.controller.rest;

import com.lancz.smartfront.backenddemo.dao.CarDAO;
import com.lancz.smartfront.backenddemo.mapper.CarMapper;
import com.lancz.smartfront.backenddemo.model.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/cars")
public class CarController {
    @Autowired
    CarDAO carDAO;

    @GetMapping("/getAll")
    public List<Car> getAllCars() {
        return carDAO.getAll();
    }

    @GetMapping("/getAllByOwner/{ownerId}")
    public List<Car> getAllByOwner(@PathVariable("ownerId") Long ownerId) {
        return carDAO.getCarsByOwnerId(ownerId);
    }
}
