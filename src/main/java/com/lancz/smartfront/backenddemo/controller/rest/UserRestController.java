package com.lancz.smartfront.backenddemo.controller.rest;

import com.lancz.smartfront.backenddemo.dao.CarDAO;
import com.lancz.smartfront.backenddemo.dao.UserDAO;
import com.lancz.smartfront.backenddemo.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private static final Logger log = LoggerFactory.getLogger(UserRestController.class);

    @Autowired private UserDAO userDAO;
    @Autowired private CarDAO carDAO;

    @GetMapping("/getAll")
    public List<User> getAll() {
        return userDAO.getAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getOneByID(@PathVariable("id") Long id) {
        User user = userDAO.findOneById(id);;

        if (user != null) {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/create")
    private User create(@RequestBody User user) {
        Long newUserId = userDAO.save(user);
        log.info("*************************** SavedUserId: " + newUserId);
        user.setId(newUserId);
        carDAO.saveUserCars(user);
        log.info("User cars saved. Num of cars: " + user.getCars().size());

        return userDAO.findOneById(newUserId);
    }
}
