package com.lancz.smartfront.backenddemo.controller.rest;

import com.lancz.smartfront.backenddemo.UserNotFoundException;
import com.lancz.smartfront.backenddemo.model.User;
import com.lancz.smartfront.backenddemo.service.report.UserCarsReportService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;

@RestController
@RequestMapping("/api")
public class ReportController {

    @Autowired private UserCarsReportService userCarsReportService;

    @Value("${generated.reports.path}")
    private String generatedReportsPath;

    @GetMapping("/users/report/download")
    public ResponseEntity<ByteArrayResource> downloadUserReport() {
        try {
            ByteArrayResource byteArrayResource = new ByteArrayResource(userCarsReportService.exportReport(null, "pdf_byte"));

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(MediaType.APPLICATION_PDF_VALUE))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=user_cars.pdf")
                    .body(byteArrayResource);

        } catch (FileNotFoundException | JRException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);

        } catch (UserNotFoundException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/users/report")
    public ResponseEntity<ByteArrayResource> UserReport() {
        return downloadUserReport();
    }

    @GetMapping("/users/report/{format}")
    public ResponseEntity<String> generateReport(@PathVariable("format") String format) {
        try {
            userCarsReportService.exportReport(null, format);

            return ResponseEntity.ok().body("Report has been generated on path: " + generatedReportsPath);
        } catch (FileNotFoundException | JRException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (UserNotFoundException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/users/{id}/report/download")
    public ResponseEntity<ByteArrayResource> getOneByIDReportDownload(@PathVariable("id") Long id) {
        try {
            ByteArrayResource byteArrayResource = new ByteArrayResource(userCarsReportService.exportReport(id, "pdf_byte"));

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(MediaType.APPLICATION_PDF_VALUE))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=user_cars.pdf")
                    .body(byteArrayResource);
        } catch (FileNotFoundException | JRException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (UserNotFoundException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/users/{id}/report")
    public ResponseEntity<ByteArrayResource> getOneByIDReport(@PathVariable("id") Long id) {
        return getOneByIDReportDownload(id);
    }

    @GetMapping("/users/{id}/report/{format}")
    public ResponseEntity<String> getOneByIDReportHtml(@PathVariable("id") Long id, @PathVariable("format") String format) {
        try {
            userCarsReportService.exportReport(id, format);

            return ResponseEntity.ok().body("Report has been generated on path: " + generatedReportsPath);
        } catch (FileNotFoundException | JRException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (UserNotFoundException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
