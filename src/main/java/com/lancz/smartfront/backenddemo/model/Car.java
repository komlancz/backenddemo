package com.lancz.smartfront.backenddemo.model;

public class Car {

    private Long id;
    private String licensePlate;
    private String color;
    private int age;
    private User owner;

    public Car() {
    }

    public Car(Long id, String licensePlate) {
        this.id = id;
        this.licensePlate = licensePlate;
    }

    public Car(Long id, String licensePlate, String color) {
        this.id = id;
        this.licensePlate = licensePlate;
        this.color = color;
    }

    public Car(Long id, String licensePlate, String color, int age) {
        this.id = id;
        this.licensePlate = licensePlate;
        this.color = color;
        this.age = age;
    }

    public Car(Long id, String licensePlate, String color, int age, User owner) {
        this.id = id;
        this.licensePlate = licensePlate;
        this.color = color;
        this.age = age;
        this.owner = owner;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
