package com.lancz.smartfront.backenddemo;

import com.lancz.smartfront.backenddemo.model.Car;
import com.lancz.smartfront.backenddemo.model.User;
import com.lancz.smartfront.backenddemo.scheduled.UserScheduler;
import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({UserScheduler.class})
public class SmartfrontBackendDemoApplication extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SmartfrontBackendDemoApplication.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(SmartfrontBackendDemoApplication.class, args);
	}
}
