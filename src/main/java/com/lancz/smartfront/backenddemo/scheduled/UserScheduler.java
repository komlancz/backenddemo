package com.lancz.smartfront.backenddemo.scheduled;

import com.lancz.smartfront.backenddemo.dao.CarDAO;
import com.lancz.smartfront.backenddemo.dao.UserDAO;
import com.lancz.smartfront.backenddemo.event.DeleteAllUserEvent;
import com.lancz.smartfront.backenddemo.model.Car;
import com.lancz.smartfront.backenddemo.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.Random;

@Configuration
@EnableScheduling
public class UserScheduler {
    Logger logger = LoggerFactory.getLogger(UserScheduler.class);

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Value("${scheduler.delete.all.user.enable}")
    private boolean deleteAllUserSchedulerEnabled;

    @Value("${scheduler.create.user.enable}")
    private boolean createUserSchedulerEnabled;

    @Autowired private UserDAO userDAO;

    @Autowired private CarDAO carDAO;

    @Scheduled(cron = "0 0/4 * 1/1 * *")
    public void deleteAllUserInEvery4Minutes() {
        if (deleteAllUserSchedulerEnabled){
            logger.info("Delete all user at " + new Date().toString());
            userDAO.deleteAll();

            DeleteAllUserEvent deleteAllUserEvent = new DeleteAllUserEvent(this);
            applicationEventPublisher.publishEvent(deleteAllUserEvent);
        }
    }

    @Scheduled(cron = "0 0/1 * 1/1 * *")
    public void createUserInEvery4Minutes() {

        if (createUserSchedulerEnabled){
            User newUser = userDAO.createNewUser();
            logger.info(String.format("New User created by id: %s at: %s", newUser.getId(), new Date()));

            Car newCar = carDAO.createNewRandomCar(newUser);
            logger.info(String.format("New Car created by id: %s at: %s", newCar.getId(), new Date()));
        }
    }
}
