package com.lancz.smartfront.backenddemo.event;

import org.springframework.context.ApplicationEvent;

public class DeleteAllUserEvent extends ApplicationEvent {

    public DeleteAllUserEvent(Object source) {
        super(source);
    }
}
