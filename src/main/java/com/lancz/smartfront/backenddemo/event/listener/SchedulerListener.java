package com.lancz.smartfront.backenddemo.event.listener;

import com.lancz.smartfront.backenddemo.event.DeleteAllUserEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

@Service
public class SchedulerListener implements ApplicationListener<DeleteAllUserEvent> {

    @Override
    public void onApplicationEvent(DeleteAllUserEvent deleteAllUserEvent) {

    }
}
