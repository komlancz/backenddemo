package com.lancz.smartfront.backenddemo.mapper;

import com.lancz.smartfront.backenddemo.model.Car;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CarMapper {

    Car getOneById(Long id);

    List<Car> getAll();

    List<Car> getAllCarAndUser();

    List<Car> getCarsByOwnerId(Long ownerId);

    void insertCar(Car car);
}
