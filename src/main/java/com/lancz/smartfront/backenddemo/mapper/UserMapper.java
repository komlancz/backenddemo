package com.lancz.smartfront.backenddemo.mapper;

import com.lancz.smartfront.backenddemo.model.Car;
import com.lancz.smartfront.backenddemo.model.User;
import org.apache.ibatis.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
public interface UserMapper {

    @Select("SELECT * FROM users")
    @Results(value = {
            @Result(property="id", column = "id"),
            @Result(property="name", column = "name"),
            @Result(property="age", column = "age"),
            @Result(property="cars", column="id", javaType= List.class, many=@Many(select="getCarsByOwnerId"))
    })
    List<User> getAll();

    @Select("INSERT INTO users(name, age) VALUES(#{name}, #{age}) RETURNING id")
    @Options(flushCache = Options.FlushCachePolicy.TRUE)
    Long insert(User user);

    @Select("SELECT * FROM users where id=#{id}")
    @Results(value = {
            @Result(property="id", column = "id"),
            @Result(property="name", column = "name"),
            @Result(property="age", column = "age"),
            @Result(property="cars", column="id", javaType= List.class, many=@Many(select="getCarsByOwnerId"))
    })
    User findOneById(Long id);

    @Delete("select * from users where id=#{id}")
    void deleteById(Long id);

    @Delete("delete from users")
    void deleteAll();

    @Select("select * from cars where owner_id=#{id}")
    @Results(value = {
            @Result(property="id", column = "id"),
            @Result(property="licensePlate", column = "license_plate"),
            @Result(property="age", column = "age"),
            @Result(property="color", column = "color")
    })
    List<Car> getCarsByOwnerId(Long id);
}
